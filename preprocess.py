import json
with open('./features.json') as f:
  data = json.load(f)

def writeToFile():
    result = dict()
    for feat in data:
        hc_key = feat["properties"]["hc-key"]
        name = feat["properties"]["name"]
        result[name]=hc_key
    with open('rdistricts.json', 'w') as outfile:
        json.dump(result, outfile)

writeToFile()