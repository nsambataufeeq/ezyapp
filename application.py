from flask import Flask,jsonify
from flask_classful import FlaskView
from flask_cors import CORS
import json

with open('./interview.json') as f:
  data = json.load(f)

with open("./rdistricts.json") as df:
    dcodes = json.load(df)

app = Flask(__name__)
CORS(app)

class DistrictView(FlaskView):
    
    route_base = "/district_view/"
    
    def index(self):
        results = list()
        for point in data:
            # get code
            dcode = dcodes[point["district"]]
            units = point["number"]
            results.append([dcode,units])
        return jsonify(results)  

DistrictView.register(app)

if __name__ == '__main__':
    app.run()
