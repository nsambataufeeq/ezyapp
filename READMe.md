## Create a virtual environment of python version 3.8+
## Install all python dependencies in requirements.txt

## API End point is only one, http://localhost:5000/district_view/
## To modularize the code flask_classy extension was added to flask.
## Serving client and backend on different origins required allow-cors headers hence FLask-CORS package.

## Ensure no service is running on port 5000.
## The data features.json  was processed using preprocess.py to obtain district codes rdistricts.json from features array http://code.highcharts.com/mapdata/countries/ug/ug-all.js

## This is so, because no district codes were provided at the time.
## Serve Front end from ezyui directory.
## Serve backend with python application.py
